package com.devsuperior.bds04.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import java.util.Arrays;

@Configuration
@EnableResourceServer
public class ResourseServerConfig extends ResourceServerConfigurerAdapter {

    @Autowired
    private Environment env;

    @Autowired
    private JwtTokenStore tokenStore;

    private static final String[] PUBLIC = {
            "/oauth/token",
            "/h2-console/**"
    };

    private static final String[] CLIENT_OR_ADMIN_GET = {
            "/cities/**",
            "/events/**"
    };

    private static final String[] CLIENT_POST = {
            "/events/**"
    };

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        //Faz a validação do Token recebido na Requisição
        resources.tokenStore(tokenStore);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {

        //Habilita o acesso ao H2 Console, quando está sendo executado o ambiente de teste
        if (Arrays.asList(env.getActiveProfiles()).contains("test")){
            http.headers().frameOptions().disable();
        }

        http.authorizeRequests()
                .antMatchers(PUBLIC).permitAll()
                .antMatchers(HttpMethod.POST, CLIENT_POST).hasAnyRole("CLIENT", "ADMIN")
                .antMatchers(HttpMethod.GET, CLIENT_OR_ADMIN_GET).permitAll()
                .anyRequest().hasAnyRole("ADMIN");
    }
}
